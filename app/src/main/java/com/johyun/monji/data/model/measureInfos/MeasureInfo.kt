package com.johyun.monji.data.model.measureInfos

/**
 * Created by JH on 2017. 6. 30..
 */

data class MeasureInfo(var dataTime: String = "",
                       var pm10Value: String = "",
                       var pm10Value24: String = "",
                       var pm25Value: String = "",
                       var pm25Value24: String = "")
