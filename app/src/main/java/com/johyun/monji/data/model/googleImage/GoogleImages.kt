package com.johyun.monji.data.model.googleImage

/**
 * Created by JH on 2017. 7. 18..
 */

data class GoogleImages(var items: MutableList<GoogleImage> = mutableListOf())
