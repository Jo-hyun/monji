package com.johyun.monji.data.database

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by JH on 2017. 7. 27..
 */

open class StationData(@PrimaryKey var stationName: String = "", var addr: String = "") : RealmObject()
