package com.johyun.monji.data.model.nearStations

/**
 * Created by JH on 2017. 6. 29..
 */

class NearStation(var stationName: String = "",
                  var addr: String = "",
                  var tm: String = "")
