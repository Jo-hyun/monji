package com.johyun.monji.data.model.stationList

data class StationList(var list: MutableList<Station> = mutableListOf(), var totalCount: String = "0")
