package com.johyun.monji.data.model.stationList

data class Station(var stationName: String = "",
                   var addr: String = "")
