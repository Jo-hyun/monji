package com.johyun.monji.data.database

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class LatestStationData(@PrimaryKey var stationName: String = "", var addr: String = "") : RealmObject()
