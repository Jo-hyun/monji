package com.johyun.monji.data.model.googleImage

data class GoogleImage(var link: String = "",
                       var image: Image = Image())
