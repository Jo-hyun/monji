package com.johyun.monji.data.model.measureInfos

/**
 * Created by JH on 2017. 6. 30..
 */

data class MeasureInfos(var list: MutableList<MeasureInfo> = mutableListOf())
