package com.johyun.monji.data.model.nearStations

data class NearStations(var list: MutableList<NearStation> = mutableListOf())
