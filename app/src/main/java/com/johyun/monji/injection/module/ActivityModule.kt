package com.johyun.monji.injection.module

import android.app.Activity

import dagger.Module
import dagger.Provides

@Module
class ActivityModule(val activity: Activity) {

    @Provides
    fun provideActivity(): Activity = activity
}
