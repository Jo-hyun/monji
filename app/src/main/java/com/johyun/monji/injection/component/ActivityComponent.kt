package com.johyun.monji.injection.component

import com.johyun.monji.injection.module.ActivityModule
import com.johyun.monji.ui.intro.IntroActivity
import com.johyun.monji.ui.main.MainActivity
import com.johyun.monji.ui.main.fragment.GpsPositionFragment
import com.johyun.monji.ui.main.fragment.ImageFragment
import com.johyun.monji.ui.main.fragment.SearchFragment

import corecommon.injector.PerActivity
import corecommon.injector.component.ApplicationComponent
import dagger.Component

@PerActivity
@Component(dependencies = arrayOf(ApplicationComponent::class), modules = arrayOf(ActivityModule::class))
interface ActivityComponent {

    fun inject(introActivity: IntroActivity)
    fun inject(mainActivity: MainActivity)
    fun inject(gpsPositionFragment: GpsPositionFragment)
    fun inject(searchFragment: SearchFragment)

    // 구글 이미지 검색
    fun inject(imageFragment: ImageFragment)

}
