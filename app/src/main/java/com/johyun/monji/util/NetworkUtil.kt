package com.johyun.monji.util

import android.content.Context
import android.net.ConnectivityManager

object NetworkUtil {

    fun isInternetConnect(context: Context): Boolean {

        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        if (activeNetwork != null) { // connected to the internet
            if (activeNetwork.type == ConnectivityManager.TYPE_WIFI || activeNetwork.type == ConnectivityManager.TYPE_MOBILE) {
                return true
            }
        }
        return false
    }
}
