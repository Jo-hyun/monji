package com.johyun.monji.util

object Define {

    //    public final static String SERVICE_KEY = "omKfin3kdSjAM9UQt7w%2FZgAS4I61RmtMRGKZt4mGzFrBakWhhuxWlpOQJEAp9LbgXsZNrtiuvbfy%2FOhq0P%2BzyQ%3D%3D"; // wemoss
    val SERVICE_KEY = "VUs43k8DDoTGGL9KCdi9S%2FB4ZDYZdHmD%2BQfAIxdnYBLZiLDLdj%2FAX2%2FbCwM0grhDxx3JEAH4%2BJGlcKyrifpnPQ%3D%3D" // webby82
    val BASE_URL = "http://openapi.airkorea.or.kr/openapi/services/rest/"

    // 대기질 측정소 정보를 조회하기 위한 서비스로 TM 좌표기반의 가까운 측정소 및 측정소 목록과 측정소의 정보를 조회할 수 있다.
    const val GET_NEAR_STATIONS = "MsrstnInfoInqireSvc/getNearbyMsrstnList"

    // 측정소명과 측정데이터 기간(일, 한달, 3개월)으로 해당 측정소의 일반항목 측정정보를 제공하는 측정소별 실시간 측정정보 조회
    const val GET_STATION_MEASURE_INFO = "ArpltnInforInqireSvc/getMsrstnAcctoRltmMesureDnsty"

    // 측정소 주소 또는 측정소 명칭으로 측정소 목록 또는 단 건의 측정소 상세 정보를 제공하는 서비스
    const val GET_STATION_LIST_BY_NAME = "MsrstnInfoInqireSvc/getMsrstnList"

    val PARAM_SERVICE_KEY = "ServiceKey"
    val PARAM_NUMOFROWS = "numOfRows"
    val PARAM_STATIONNAME = "stationName"
    val PARAM_RETURNTYPE = "_returnType"
    val PARAM_DATATERM = "dataTerm"
    val PARAM_VER = "ver"
    val PARAM_TMX = "tmX"
    val PARAM_TMY = "tmY"

    val DATATERM_VALUE = "DAILY"
    val JSON = "json"
    val VER = "1.3"

    val PERMISSIONS_ACCESS_FINE_LOCATION_DENY_TEXT = "기기의 위치정보 액세스 권한을 획득하지 못해 위치정보가 필요한 기능을 사용할 수 없습니다."

    val PERMISSIONS_ACCESS_FINE_LOCATION_CODE = 1000

    val GPS_INTERVAL = (1000 * 10).toLong()
    val GPS_FAST_INTERVAL = (1000 * 5).toLong()
    val BACKGROUND_TRANSITION_DURATION = 1000

    val TEXT_ANIMATION_TIME = 1500


    // 구글 이미지 검색
    const val GOOGLE_IMAGE_SEARCH_BASE_URL = "https://www.googleapis.com/customsearch/v1"
    val PARAM_GOOGLE_IMAGE_SEARCH_Q = "q"
    val PARAM_GOOGLE_IMAGE_SEARCH_KEY = "key"
    val PARAM_GOOGLE_IMAGE_SEARCH_CX = "cx"
    val PARAM_GOOGLE_IMAGE_SEARCH_SEARCHTYPE = "searchType"
    val PARAM_GOOGLE_IMAGE_SEARCH_IMAGE_SIZE = "imgSize"
    val PARAM_GOOGLE_IMAGE_SEARCH_FILETYPE = "fileType"

    val PARAM_GOOGLE_IMAGE_SEARCH_KEY_VALUE = "AIzaSyCYu1PCAcuSQId7LNim7QLzft5PrpyiMgU"
    val PARAM_GOOGLE_IMAGE_SEARCH_CX_VALUE = "013625891224017418565:hlc0tygdyag"
    val PARAM_GOOGLE_IMAGE_SEARCH_SEARCHTYPE_VALUE = "image"
    val PARAM_GOOGLE_IMAGE_SEARCH_IMAGE_SIZE_VALUE = "large"
    val PARAM_GOOGLE_IMAGE_SEARCH_FILETYPE_VALUE = "png"

}
