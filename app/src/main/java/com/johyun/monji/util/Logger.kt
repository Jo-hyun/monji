package com.johyun.monji.util

import android.util.Log

object Logger {
    private val isDebug = true
    private val Monji = "Monji "

    fun d(TAG: String, message: String) {
        if (isDebug) Log.d(TAG, Monji + message)
    }

    fun i(TAG: String, message: String) {
        if (isDebug) Log.i(TAG, Monji + message)
    }

    fun e(TAG: String, message: String) {
        if (isDebug) Log.e(TAG, Monji + message)
    }

    fun w(TAG: String, message: String) {
        if (isDebug) Log.w(TAG, Monji + message)
    }

    fun v(TAG: String, message: String) {
        if (isDebug) Log.v(TAG, Monji + message)
    }
}
