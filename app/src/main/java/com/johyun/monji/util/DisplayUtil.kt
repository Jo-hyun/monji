package com.johyun.monji.util

import android.content.res.Resources

object DisplayUtil {
    private val xDisplayMetrics = Resources.getSystem().displayMetrics
    private val density = xDisplayMetrics.density

    fun px2dp(px: Int): Int = (px / density).toInt()

    fun dp2px(dp: Int): Int = (dp * density).toInt()

    fun px2dp(px: Float): Int = (px / density).toInt()

    fun dp2px(dp: Float): Int = (dp * density).toInt()

    fun densityDpi(): Int = xDisplayMetrics.densityDpi

    fun dpi(): Float = xDisplayMetrics.densityDpi.toFloat()

    fun screenWidth(): Int = xDisplayMetrics.widthPixels

    fun screenHeight(): Int = xDisplayMetrics.heightPixels
}
