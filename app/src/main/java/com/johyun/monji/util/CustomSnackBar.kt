package com.johyun.monji.util

import android.os.Handler
import android.support.design.widget.Snackbar
import android.view.View

class CustomSnackBar(private val view: View, private val content: String) {
    private lateinit var snackbar: Snackbar

    fun show() {
        snackbar = Snackbar.make(view, content, Snackbar.LENGTH_LONG)
        snackbar.show()
    }

    fun showWithAction(btnText: String, runnable: Runnable) {
        snackbar = Snackbar.make(view, content, Snackbar.LENGTH_INDEFINITE)
        snackbar.setAction(btnText) {
            snackbar.dismiss()
            Handler().post(runnable)
        }
        snackbar.show()
    }
}
