package com.johyun.monji.util

import android.content.Context
import android.content.SharedPreferences

object PreferencesUtil {
    private var sharedPreferences: SharedPreferences? = null
    private var sharedPreferencesEditor: SharedPreferences.Editor? = null

    private fun getSharedPreferences(context: Context): SharedPreferences? {
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
        }
        return sharedPreferences
    }

    private fun getSharedPreferencesEditor(context: Context): SharedPreferences.Editor? {
        if (sharedPreferencesEditor == null) {
            sharedPreferencesEditor = getSharedPreferences(context)!!.edit()
        }
        return sharedPreferencesEditor
    }

    fun getValue(context: Context, key: String, defaultValue: Any): Any? {
        var returnValue: Any? = null
        when (defaultValue) {
            is Boolean -> returnValue = getSharedPreferences(context)!!.getBoolean(key, defaultValue)
            is String -> returnValue = getSharedPreferences(context)!!.getString(key, defaultValue)
            is Float -> returnValue = getSharedPreferences(context)!!.getFloat(key, defaultValue)
            is Int -> returnValue = getSharedPreferences(context)!!.getInt(key, defaultValue)
            is Long -> returnValue = getSharedPreferences(context)!!.getLong(key, defaultValue)
        }
        return returnValue
    }

    fun putValue(context: Context, key: String, value: Any) {
        when (value) {
            is Boolean -> getSharedPreferencesEditor(context)!!.putBoolean(key, value)
            is String -> getSharedPreferencesEditor(context)!!.putString(key, value)
            is Float -> getSharedPreferencesEditor(context)!!.putFloat(key, value)
            is Int -> getSharedPreferencesEditor(context)!!.putInt(key, value)
            is Long -> getSharedPreferencesEditor(context)!!.putLong(key, value)
        }
    }
}