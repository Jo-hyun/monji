package com.johyun.monji.util

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.support.v4.content.ContextCompat

object ResUtil {
    fun getColor(context: Context, id: Int): Int {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) { // 23
            ContextCompat.getColor(context, id)
        } else {
            context.resources.getColor(id)
        }
    }

    fun getDrawable(context: Context, id: Int): Drawable? {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { // 21
            context.getDrawable(id)
        } else {
            context.resources.getDrawable(id)
        }
    }

    fun getString(context: Context, id: Int): String = context.resources.getString(id)
}
