package com.johyun.monji.ui.main.fragment

import android.Manifest
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.pm.PackageManager
import android.graphics.drawable.TransitionDrawable
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.ContentViewEvent
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.johyun.monji.R
import com.johyun.monji.ui.base.BaseActivity
import com.johyun.monji.ui.base.BaseFragment
import com.johyun.monji.ui.main.viewUtil.MeasureDataSetting
import com.johyun.monji.util.*
import com.johyun.monji.util.Geo.GeoPoint
import com.johyun.monji.util.Geo.GeoTrans
import corecommon.ui.main.fragment.GpsPositionFragmentView
import kotlinx.android.synthetic.main.fragment_gps_position.*
import javax.inject.Inject

class GpsPositionFragment : BaseFragment(), GpsPositionFragmentView, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private val TAG = GpsPositionFragment::class.java.simpleName

    @Inject lateinit var gpsPositionFragmentPresenter: GpsPositionFragmentPresenter

    private lateinit var locationRequest: LocationRequest
    private lateinit var googleApiClient: GoogleApiClient
    private var beforeValue = 0
    private var isClickable = true
    private var isFirst = true
    private var requestPermissionCount = 0

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater!!.inflate(R.layout.fragment_gps_position, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as BaseActivity).activityComponent.inject(this)
        gpsPositionFragmentPresenter.attachView(this)

        chkNetwork()
        rl_info_wrapper.visibility = View.GONE
        tv_status.visibility = View.GONE
    }

    override fun showProgress() {
        avi.smoothToShow()
    }

    override fun hideProgress() {
        avi.smoothToHide()
    }

    override fun showErrorAlert(message: String) {
        CustomSnackBar(rl_background, message).showWithAction("OK", Runnable { requestGpsLocation() })
    }

    override fun showFinishAlert(message: String) {
        CustomSnackBar(rl_background, message).showWithAction("OK", Runnable { activity.finish() })
    }

    private fun chkNetwork() {
        if (NetworkUtil.isInternetConnect(context)) {
            initAll()
        } else {
            CustomSnackBar(rl_background, ResUtil.getString(context, R.string.connect_fail_network)).showWithAction("OK", Runnable { chkNetwork() })
        }
    }

    private fun initAll() {
        googleApiClient = GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()

        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(Define.GPS_INTERVAL)
                .setFastestInterval(Define.GPS_FAST_INTERVAL)

        rl_background.setOnClickListener {
            if (isClickable) {
                resetUiAndRequestGps()
                isClickable = false
            }
        }

        requestGpsLocation()
    }

    private fun resetUiAndRequestGps() {
        rl_info_wrapper.visibility = View.GONE
        tv_status.visibility = View.GONE
        requestGpsLocation()
    }

    override fun onConnected(bundle: Bundle?) {
        requestGpsLocation()
        Logger.d(TAG, "googleApiClient onConnected")
    }

    override fun onLocationChanged(location: Location) {
        Logger.d(TAG, "googleApiClient onLocationChanged")

        val in_pt = GeoPoint(location.longitude, location.latitude)
        val tm_pt = GeoTrans.convert(GeoTrans.GEO, GeoTrans.TM, in_pt)
        Logger.d(TAG, "tm_pt.getX() = ${tm_pt.x}")
        Logger.d(TAG, "tm_pt.getY() = ${tm_pt.y}")

        gpsPositionFragmentPresenter.getNearStation(tm_pt.x, tm_pt.y)
        removeLocationUpdate()
    }

    override fun onConnectionSuspended(i: Int) {

    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {

    }

    private fun connectGoogleApiClient() {
        if (!googleApiClient.isConnected) {
            googleApiClient.connect()
            Logger.d(TAG, "connectGoogleApiClient()")
        }
    }

    private fun disconnectGoogleApiClient() {
        if (googleApiClient.isConnected) {
            googleApiClient.disconnect()
            Logger.d(TAG, "disconnectGoogleApiClient()")
        }
    }

    private fun removeLocationUpdate() {
        if (googleApiClient.isConnected) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this)
        }

        Logger.d(TAG, "removeLocationUpdate()")
    }

    private fun requestGpsLocation() {
        if (checkPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)) {
            if (googleApiClient.isConnected) {
                LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this)
            } else {
                connectGoogleApiClient()
            }
        } else {
            requestPermissionCount += 1
            requestPermission(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), Define.PERMISSIONS_ACCESS_FINE_LOCATION_CODE)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Logger.d(TAG, "grantResults.length > 0 = ${grantResults.isNotEmpty()}")
            Logger.d(TAG, "grantResults[0] == PackageManager.PERMISSION_GRANTED = ${grantResults[0] == PackageManager.PERMISSION_GRANTED}")
            Logger.d(TAG, "requestCode = $requestCode")

            when (requestCode) {
                Define.PERMISSIONS_ACCESS_FINE_LOCATION_CODE -> {
                    Logger.d(TAG, "PERMISSIONS_ACCESS_FINE_LOCATION_CODE")
                    connectGoogleApiClient()
                }
            }
        } else {
            if (requestPermissionCount < 3) {
                CustomSnackBar(rl_background, Define.PERMISSIONS_ACCESS_FINE_LOCATION_DENY_TEXT).show()
            } else {
                activity.finish()
            }
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        Logger.d(TAG, "isVisibleToUser = $isVisibleToUser")

        if (!isFirst && isVisibleToUser) resetUiAndRequestGps()
    }

    override fun onResume() {
        // activity onResume 이 불릴때만 불림
        // 처음 한번과 앱이 내렸갔다 올라올때 불림
        super.onResume()
        Logger.d(TAG, "onResume()")

        if (!isFirst) resetUiAndRequestGps()
        isFirst = false

        MobileAds.initialize(context, ResUtil.getString(context, R.string.banner_ad_unit_id_gps))
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }

    override fun changeColor(station: String, measureTime: String, pm10: String, pm25: String) {
        val currValue = getStatus(pm10)
        val runnable = Runnable {
            hideProgress()
            rl_info_wrapper.visibility = View.VISIBLE
            tv_status.visibility = View.VISIBLE

            if (currValue == 0) {
                tv_pm10.text = "..."
                tv_pm25.text = "..."
            } else {
                setMeasurePm10Info(pm10)
                setMeasurePm25Info(pm25)
            }

            tv_station.text = "$station ${ResUtil.getString(context, R.string.append_after_station_name)}"
            tv_time.text = measureTime

            when (currValue) {
                0 -> tv_status.text = ResUtil.getString(context, R.string.nothing)
                1 -> tv_status.text = ResUtil.getString(context, R.string.good)
                2 -> tv_status.text = ResUtil.getString(context, R.string.soso)
                3 -> tv_status.text = ResUtil.getString(context, R.string.bad)
                4 -> tv_status.text = ResUtil.getString(context, R.string.hell)
            }
            setMeasureStatusAlpha()

            val background = rl_background.background as TransitionDrawable
            background.startTransition(Define.BACKGROUND_TRANSITION_DURATION)

            beforeValue = currValue
            Handler().postDelayed({ isClickable = true }, 1000)

            Answers.getInstance().logContentView(ContentViewEvent()
                    .putCustomAttribute("GPS_region", station)
                    .putCustomAttribute(station + "_pm10", pm10)
                    .putCustomAttribute(station + "_pm25", pm25)
            )
        }

        MeasureDataSetting(context, beforeValue, currValue, rl_background, runnable)
    }

    private fun setMeasureStatusAlpha() {
        val anim = ObjectAnimator.ofFloat(tv_status, View.ALPHA, 0f, 1f)
        anim.duration = Define.TEXT_ANIMATION_TIME.toLong()
        anim.start()
    }

    private fun setMeasurePm10Info(pm10: String) {
        if (pm10.equals("-", ignoreCase = true)) {
            tv_pm10.text = pm10
        } else {
            val animator = ValueAnimator.ofInt(0, Integer.parseInt(pm10))
            animator.duration = Define.TEXT_ANIMATION_TIME.toLong()
            animator.addUpdateListener { animation -> tv_pm10.text = animation.animatedValue.toString() }
            animator.start()
        }
    }

    private fun setMeasurePm25Info(pm25: String) {
        if (pm25.equals("-", ignoreCase = true)) {
            tv_pm25.text = pm25
        } else {
            val animator = ValueAnimator.ofInt(0, Integer.parseInt(pm25))
            animator.duration = Define.TEXT_ANIMATION_TIME.toLong()
            animator.addUpdateListener { animation -> tv_pm25.text = animation.animatedValue.toString() }
            animator.start()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disconnectGoogleApiClient()
    }
}
