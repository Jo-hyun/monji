package com.johyun.monji.ui.main

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

import com.johyun.monji.ui.main.fragment.GpsPositionFragment
import com.johyun.monji.ui.main.fragment.ImageFragment
import com.johyun.monji.ui.main.fragment.SearchFragment

class MainViewPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    private val PAGE_COUNT: Int = 2
    private val gpsPositionFragment = GpsPositionFragment()
    private val searchFragment = SearchFragment()
    private val imageFragment = ImageFragment()

    init {
        var bundle = Bundle()
        bundle.putInt("index", 0)
        gpsPositionFragment.arguments = bundle

        bundle = Bundle()
        bundle.putInt("index", 1)
        searchFragment.arguments = bundle

        bundle = Bundle()
        bundle.putInt("index", 2)
        imageFragment.arguments = bundle
    }

    override fun getItem(position: Int): Fragment? {
        when (position) {
            0 -> return gpsPositionFragment
            1 -> return searchFragment
//            2 -> return imageFragment
            else -> return null
        }
    }

    override fun getCount(): Int = PAGE_COUNT
}
