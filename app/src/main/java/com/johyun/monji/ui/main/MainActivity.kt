package com.johyun.monji.ui.main

import android.os.Bundle
import com.johyun.monji.R
import com.johyun.monji.ui.base.BaseActivity
import com.johyun.monji.util.Logger
import com.johyun.monji.util.PreferencesUtil
import corecommon.ui.main.MainMvpView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), MainMvpView {

    private val TAG = MainActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Logger.d(TAG, "onCreate")
        setContentView(R.layout.activity_main)
        activityComponent.inject(this)

        vp_main.adapter = MainViewPagerAdapter(supportFragmentManager)
        vp_main.offscreenPageLimit = 2

        PreferencesUtil.putValue(applicationContext, "StringPut", "StringValue")
        PreferencesUtil.putValue(applicationContext, "BooleanPut", false)
        PreferencesUtil.putValue(applicationContext, "FloatPut", 100.0f)
        PreferencesUtil.putValue(applicationContext, "IntPut", 5000)
        PreferencesUtil.putValue(applicationContext, "LongPut", 3333333)

        Logger.d(TAG, "StringPut = ${PreferencesUtil.getValue(applicationContext, "StringPut", false)}")
    }
}
