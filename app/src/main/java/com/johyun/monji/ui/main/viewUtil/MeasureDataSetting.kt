package com.johyun.monji.ui.main.viewUtil

import android.content.Context
import android.os.Handler
import android.view.View
import com.johyun.monji.R
import com.johyun.monji.util.Logger
import com.johyun.monji.util.ResUtil

class MeasureDataSetting(private val context: Context,
                         private val beforeValue: Int,
                         private val currValue: Int,
                         private val background: View,
                         private val runnable: Runnable) {

    private val TAG = MeasureDataSetting::class.java.simpleName

    init {
        setInformation()
    }

    private fun setInformation() {

        Logger.d(TAG, "beforeValue = $beforeValue, currValue = $currValue")

        when (beforeValue) {
            0 -> when (currValue) {
                0 -> setBackground(R.drawable.transition_0to0)
                1 -> setBackground(R.drawable.transition_0to1)
                2 -> setBackground(R.drawable.transition_0to2)
                3 -> setBackground(R.drawable.transition_0to3)
                4 -> setBackground(R.drawable.transition_0to4)
            }
            1 -> when (currValue) {
                0 -> setBackground(R.drawable.transition_1to0)
                1 -> setBackground(R.drawable.transition_1to1)
                2 -> setBackground(R.drawable.transition_1to2)
                3 -> setBackground(R.drawable.transition_1to3)
                4 -> setBackground(R.drawable.transition_1to4)
            }
            2 -> when (currValue) {
                0 -> setBackground(R.drawable.transition_2to0)
                1 -> setBackground(R.drawable.transition_2to1)
                2 -> setBackground(R.drawable.transition_2to2)
                3 -> setBackground(R.drawable.transition_2to3)
                4 -> setBackground(R.drawable.transition_2to4)
            }
            3 -> when (currValue) {
                0 -> setBackground(R.drawable.transition_3to0)
                1 -> setBackground(R.drawable.transition_3to1)
                2 -> setBackground(R.drawable.transition_3to2)
                3 -> setBackground(R.drawable.transition_3to3)
                4 -> setBackground(R.drawable.transition_3to4)
            }
            4 -> when (currValue) {
                0 -> setBackground(R.drawable.transition_4to0)
                1 -> setBackground(R.drawable.transition_4to1)
                2 -> setBackground(R.drawable.transition_4to2)
                3 -> setBackground(R.drawable.transition_4to3)
                4 -> setBackground(R.drawable.transition_4to4)
            }
        }
        Handler().postDelayed(runnable, 1000)
    }

    private fun setBackground(id: Int) {
        background.background = ResUtil.getDrawable(context, id)
    }
}
