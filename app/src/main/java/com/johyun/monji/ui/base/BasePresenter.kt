package com.johyun.monji.ui.base

import corecommon.ui.base.MvpView
import corecommon.ui.base.Presenter

open class BasePresenter<T : MvpView> : Presenter<T> {

    private var mMvpView: T? = null

    override fun attachView(mvpView: T) {
        mMvpView = mvpView
    }

    override fun detachView() {
        mMvpView = null
    }

    val isViewAttached: Boolean
        get() = mMvpView != null

    val mvpView: T?
        get() {
            checkViewAttached()
            return mMvpView
        }

    fun checkViewAttached() {
        if (!isViewAttached) {
            throw MvpViewNotAttachedException()
        }
    }

    class MvpViewNotAttachedException : RuntimeException("Please call Presenter.attachView(MvpView) before requesting data to the Presenter")
}
