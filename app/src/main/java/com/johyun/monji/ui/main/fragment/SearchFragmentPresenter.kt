package com.johyun.monji.ui.main.fragment

import android.content.Context
import com.johyun.monji.R
import com.johyun.monji.data.database.LatestStationData
import com.johyun.monji.data.database.StationData
import com.johyun.monji.data.model.measureInfos.MeasureInfos
import com.johyun.monji.ui.base.BasePresenter
import com.johyun.monji.util.Logger
import com.johyun.monji.util.ResUtil
import corecommon.data.DataManager
import corecommon.data.database.RealmService
import corecommon.data.http.ExceptionHandler
import corecommon.ui.main.fragment.SearchFragmentView
import rx.SingleSubscriber
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SearchFragmentPresenter
@Inject constructor(private val dataManager: DataManager,
                    private val realmService: RealmService,
                    private val exceptionHandler: ExceptionHandler) : BasePresenter<SearchFragmentView>() {

    private val TAG = SearchFragmentPresenter::class.java.simpleName
    private var subscription: Subscription? = null
    private var failCount503 = 0

    override fun detachView() {
        super.detachView()

        subscription!!.unsubscribe()
        realmService.closeRealm()
    }

    fun getMeasureInfos(context: Context, stationName: String, addr: String) {
        mvpView!!.showProgress()

        subscription = dataManager.getMeasureInfos(stationName)
                .timeout(20, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleSubscriber<MeasureInfos>() {
                    override fun onSuccess(measureInfos: MeasureInfos) {
                        Logger.d(TAG, measureInfos.list.toString())

                        if (measureInfos.list.isEmpty()) {
                            mvpView!!.showErrorAlert(ResUtil.getString(context, R.string.search_result_nothing))
                            mvpView!!.hideProgress()
                        } else {
                            // insert selected item to db
                            Logger.d(TAG, "stationName = $stationName")
                            val latestStationData = LatestStationData()
                            latestStationData.stationName = stationName
                            latestStationData.addr = addr
                            realmService.insertLatestStationData(latestStationData)

                            mvpView!!.changeColor(
                                    stationName,
                                    measureInfos.list[0].dataTime,
                                    measureInfos.list[0].pm10Value,
                                    measureInfos.list[0].pm25Value
                            )
                        }
                    }

                    override fun onError(error: Throwable) {
                        mvpView!!.hideProgress()
                        Logger.d(TAG, error.toString())

                        exceptionHandler.handleHttpException(error,
                                object : ExceptionHandler.ExceptHandleListener {
                                    override fun onTimeoutException(errorMessage: String) {
                                        mvpView!!.showErrorAlert(errorMessage)
                                    }

                                    override fun onException(errorMessage: String) {
                                        mvpView!!.showErrorAlert(errorMessage)
                                    }

                                    override fun on503Exception(errorMessage: String) {
                                        when (failCount503) {
                                            0 -> {
                                                mvpView!!.showErrorAlert(errorMessage)
                                                failCount503 += 1
                                            }
                                            1 -> {
                                                mvpView!!.showErrorAlert(errorMessage)
                                                failCount503 += 1
                                            }
                                            2 -> mvpView!!.showFinishAlert(errorMessage)
                                        }
                                    }
                                })
                    }
                })
    }

    fun getStationListByName(stationName: String): MutableList<StationData> = realmService.getStationList(stationName)

    fun getAllLatestStationData(): List<LatestStationData> = realmService.allLatestStationList

    fun removeAllLatestStationData(): Int = realmService.deleteAllLatestStation()

    fun removeLatestStationData(position: Int) {
        realmService.deleteLatestStation(position)
    }
}
