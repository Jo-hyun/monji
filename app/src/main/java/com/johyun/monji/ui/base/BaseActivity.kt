package com.johyun.monji.ui.base

import android.content.Context
import android.support.v7.app.AppCompatActivity
import com.johyun.monji.injection.component.ActivityComponent
import com.johyun.monji.injection.component.DaggerActivityComponent
import com.tsengvn.typekit.TypekitContextWrapper
import corecommon.MonjiApplication

/**
 * Created by JH on 2017. 7. 25..
 */
open class BaseActivity : AppCompatActivity() {

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase))
    }

    val activityComponent: ActivityComponent by lazy {
        DaggerActivityComponent
                .builder()
                .applicationComponent(MonjiApplication.get(applicationContext).applicationComponent)
                .build()
    }
}