package com.johyun.monji.ui.main.fragment

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.johyun.monji.R
import com.johyun.monji.data.database.StationData
import com.johyun.monji.util.Logger
import java.util.*

class SearchAdapter(val context: Context) : RecyclerView.Adapter<SearchAdapter.ViewHolder>() {

    private val TAG = SearchAdapter::class.java.simpleName

    private lateinit var stationList: MutableList<StationData>
    private val mInputMethodManager: InputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    private var isLatest = false
    private lateinit var itemClick: ItemClick
    private lateinit var deleteClick: DeleteClick

    interface ItemClick {
        fun onClick(view: View, position: Int, stationName: String, addr: String)
    }

    interface DeleteClick {
        fun onClick(view: View, position: Int)
    }

    fun setItemClick(itemClick: ItemClick) {
        this.itemClick = itemClick
    }

    fun setDeleteClick(deleteClick: DeleteClick) {
        this.deleteClick = deleteClick
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvStationName: TextView = view.findViewById(R.id.tv_station_name)
        var tvAddr: TextView = view.findViewById(R.id.tv_addr)
        var rlWrapper: RelativeLayout = view.findViewById(R.id.rl_wrapper)
        var ivRemoveItem: ImageView = view.findViewById(R.id.iv_remove_item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_search, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if (stationList.isNotEmpty()) {
            val stationData = stationList[position]

            Logger.d(TAG, "stationName = ${stationData.stationName}")
            holder.tvStationName.text = stationData.stationName

            holder.rlWrapper.setOnClickListener { v ->
                itemClick.onClick(v, position, stationData.stationName, stationData.addr)
                mInputMethodManager.hideSoftInputFromWindow(v.windowToken, 0)
            }

            if (isLatest) {
                holder.tvAddr.visibility = View.INVISIBLE
                holder.ivRemoveItem.visibility = View.VISIBLE

                holder.ivRemoveItem.setOnClickListener { v ->
                    deleteClick.onClick(v, position)
                    mInputMethodManager.hideSoftInputFromWindow(v.windowToken, 0)
                }

            } else {
                holder.tvAddr.visibility = View.VISIBLE
                holder.ivRemoveItem.visibility = View.INVISIBLE

                holder.tvAddr.text = stationData.addr
            }
        }
    }

    override fun getItemCount(): Int = stationList.size

    fun updateStationList(stationList: MutableList<StationData>, isLatest: Boolean) {
        this.stationList = stationList
        this.isLatest = isLatest
        for (station in stationList) {
            Logger.d(TAG, "stationName = ${station.stationName}")
        }

        notifyDataSetChanged()
    }

    fun removeItem(position: Int) {
        stationList.removeAt(position)
        notifyItemRemoved(position)
    }

    fun removeAllItem(size: Int) {
        stationList = ArrayList<StationData>()
        notifyItemRangeRemoved(0, size)
    }
}
