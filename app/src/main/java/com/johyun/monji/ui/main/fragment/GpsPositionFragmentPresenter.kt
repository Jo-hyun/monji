package com.johyun.monji.ui.main.fragment

import com.johyun.monji.data.model.measureInfos.MeasureInfos
import com.johyun.monji.data.model.nearStations.NearStations
import com.johyun.monji.ui.base.BasePresenter
import com.johyun.monji.util.Logger
import corecommon.data.DataManager
import corecommon.data.database.RealmService
import corecommon.data.http.ExceptionHandler
import corecommon.ui.main.fragment.GpsPositionFragmentView
import rx.SingleSubscriber
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class GpsPositionFragmentPresenter
@Inject constructor(private val dataManager: DataManager,
                    private val realmService: RealmService,
                    private val exceptionHandler: ExceptionHandler) : BasePresenter<GpsPositionFragmentView>() {

    private val TAG = GpsPositionFragmentPresenter::class.java.simpleName
    private var subscription: Subscription? = null
    private var failCount503 = 0

    override fun detachView() {
        super.detachView()

        subscription!!.unsubscribe()
        realmService.closeRealm()
    }

    fun getNearStation(tmX: Double, tmY: Double) {
        mvpView!!.showProgress()

        subscription = dataManager.getNearStations(tmX, tmY)
                .timeout(20, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleSubscriber<NearStations>() {
                    override fun onSuccess(nearStations: NearStations) {
                        Logger.d(TAG, "nearStations.list = ${nearStations.list}")

                        getMeasureInfos(nearStations.list[0].stationName)
                    }

                    override fun onError(error: Throwable) {
                        mvpView!!.hideProgress()

                        exceptionHandler.handleHttpException(error,
                                object : ExceptionHandler.ExceptHandleListener {
                                    override fun onTimeoutException(errorMessage: String) {
                                        mvpView!!.showErrorAlert(errorMessage)
                                    }

                                    override fun onException(errorMessage: String) {
                                        mvpView!!.showErrorAlert(errorMessage)
                                    }

                                    override fun on503Exception(errorMessage: String) {
                                        when (failCount503) {
                                            0 -> {
                                                mvpView!!.showErrorAlert(errorMessage)
                                                failCount503 += 1
                                            }
                                            1 -> {
                                                mvpView!!.showErrorAlert(errorMessage)
                                                failCount503 += 1
                                            }
                                            2 -> mvpView!!.showFinishAlert(errorMessage)
                                        }
                                    }
                                })
                    }
                })
    }

    private fun getMeasureInfos(stationName: String) {

        subscription = dataManager.getMeasureInfos(stationName)
                .timeout(20, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleSubscriber<MeasureInfos>() {
                    override fun onSuccess(measureInfos: MeasureInfos) {
                        Logger.d(TAG, "measureInfos.list = ${measureInfos.list}")

                        mvpView!!.changeColor(
                                stationName,
                                measureInfos.list[0].dataTime,
                                measureInfos.list[0].pm10Value,
                                measureInfos.list[0].pm25Value
                        )
                    }

                    override fun onError(error: Throwable) {
                        mvpView!!.hideProgress()
                        Logger.d(TAG, error.toString())

                        exceptionHandler.handleHttpException(error,
                                object : ExceptionHandler.ExceptHandleListener {
                                    override fun onTimeoutException(errorMessage: String) {
                                        mvpView!!.showErrorAlert(errorMessage)
                                    }

                                    override fun onException(errorMessage: String) {
                                        mvpView!!.showErrorAlert(errorMessage)
                                    }

                                    override fun on503Exception(errorMessage: String) {
                                        when (failCount503) {
                                            0 -> {
                                                mvpView!!.showErrorAlert(errorMessage)
                                                failCount503 += 1
                                            }
                                            1 -> {
                                                mvpView!!.showErrorAlert(errorMessage)
                                                failCount503 += 1
                                            }
                                            2 -> mvpView!!.showFinishAlert(errorMessage)
                                        }
                                    }
                                })
                    }
                })
    }
}
