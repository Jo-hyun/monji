package com.johyun.monji.ui.base

import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.Fragment

import android.support.v4.content.PermissionChecker.checkSelfPermission

/**
 * Created by JH on 2017. 7. 7..
 */

open class BaseFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    protected fun checkPermission(context: Context, permission: String): Boolean {
        return checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED
    }

    protected fun requestPermission(permissions: Array<String>, requestCode: Int) {
        requestPermissions(permissions, requestCode)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    protected fun getStatus(pm10: String): Int {
        var status: Int = 0

        if (!pm10.equals("-", ignoreCase = true)) {
            val pm10Int = Integer.parseInt(pm10)

            when {
                pm10Int > 250 -> status = 4
                pm10Int in 101..250 -> status = 3
                pm10Int in 51..100 -> status = 2
                else -> status = 1
            }
        }

        return status
    }
}
