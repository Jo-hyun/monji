package com.johyun.monji.ui.main.fragment

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.johyun.monji.R
import com.johyun.monji.data.model.googleImage.GoogleImage
import com.johyun.monji.util.Logger
import java.util.*

class ImageAdapter(private val context: Context) : RecyclerView.Adapter<ImageAdapter.ViewHolder>() {

    private val TAG = ImageAdapter::class.java.simpleName
    private var imageList: List<GoogleImage> = ArrayList()

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var iv_image: ImageView = view.findViewById(R.id.iv_image)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_image, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (imageList.isNotEmpty()) {
            val googleImage = imageList[position]
            Logger.d(TAG, "getThumbnailLink = " + googleImage.image.thumbnailLink)

//            Glide.with(context)
//                    .load(googleImage.image.thumbnailLink)
//                    .placeholder(R.drawable.btn_close_black)
//                    .into(holder.iv_image)
        }

    }

    override fun getItemCount(): Int = imageList.size

    fun updateImageList(imageList: List<GoogleImage>) {
        this.imageList = imageList
        notifyDataSetChanged()
    }
}
