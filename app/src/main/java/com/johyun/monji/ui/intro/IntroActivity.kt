package com.johyun.monji.ui.intro

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.johyun.monji.R
import com.johyun.monji.ui.base.BaseActivity
import com.johyun.monji.ui.main.MainActivity
import com.johyun.monji.util.CustomSnackBar
import corecommon.ui.intro.IntroMvpView
import kotlinx.android.synthetic.main.activity_intro.*
import javax.inject.Inject

class IntroActivity : BaseActivity(), IntroMvpView {
    private val TAG = IntroActivity::class.java.simpleName

    @Inject
    lateinit var introPresenter: IntroPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intro)
        activityComponent.inject(this)
        introPresenter.attachView(this)
    }

    override fun onResume() {
        super.onResume()
        introPresenter.getStationListCount(this)
    }

    override fun showProgress() {
        avi.smoothToShow()
    }

    override fun hideProgress() {
        avi.smoothToHide()
    }

    override fun showErrorAlert(message: String) {
        val snackBar = CustomSnackBar(rl_background, message)
        snackBar.showWithAction("OK", Runnable { introPresenter.getStationListCount(this) })
    }

    override fun showFinishAlert(message: String) {
        val snackBar = CustomSnackBar(rl_background, message)
        snackBar.showWithAction("OK", Runnable { finish() })
    }

    override fun goMainActivity() {
        Handler().postDelayed({
            hideProgress()
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }, 1500)
    }
}
