package com.johyun.monji.ui.main.fragment

import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.drawable.TransitionDrawable
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.ContentViewEvent
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.johyun.monji.R
import com.johyun.monji.data.database.StationData
import com.johyun.monji.ui.base.BaseActivity
import com.johyun.monji.ui.base.BaseFragment
import com.johyun.monji.ui.main.viewUtil.MeasureDataSetting
import com.johyun.monji.util.*
import corecommon.ui.main.fragment.SearchFragmentView
import kotlinx.android.synthetic.main.fragment_search.*
import java.util.*
import javax.inject.Inject

class SearchFragment : BaseFragment(), SearchFragmentView {

    private val TAG = SearchFragment::class.java.simpleName

    @Inject lateinit var searchFragmentPresenter: SearchFragmentPresenter
    private lateinit var searchAdapter: SearchAdapter
    private lateinit var lastStationName: String
    private lateinit var lastAddr: String
    private lateinit var imm: InputMethodManager
    private var beforeValue: Int = 0

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater!!.inflate(R.layout.fragment_search, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as BaseActivity).activityComponent.inject(this)
        searchFragmentPresenter.attachView(this)
        imm = (activity.getSystemService(Context.INPUT_METHOD_SERVICE)) as InputMethodManager
        hideProgress()
        chkNetwork()
    }

    override fun onResume() {
        // activity onResume 이 불릴때만 불림
        // 처음 한번과 앱이 내렸갔다 올라올때 불림
        super.onResume()

        MobileAds.initialize(context, ResUtil.getString(context, R.string.banner_ad_unit_id_gps))
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }

    private fun chkNetwork() {
        if (!NetworkUtil.isInternetConnect(context)) {
            CustomSnackBar(rl_background, ResUtil.getString(context, R.string.connect_fail_network)).showWithAction("OK", Runnable { chkNetwork() })
        } else {
            initView()
        }
    }

    private fun initView() {
        searchAdapter = SearchAdapter(context)
        rv_search_station_list.layoutManager = LinearLayoutManager(context)
        rv_search_station_list.setHasFixedSize(true)
        rv_search_station_list.adapter = searchAdapter

        searchAdapter.setItemClick(object : SearchAdapter.ItemClick {
            override fun onClick(view: View, position: Int, stationName: String, addr: String) {
                lastStationName = stationName
                lastAddr = addr

                tv_pm10.text = ""
                tv_pm25.text = ""
                tv_station.text = ""
                tv_time.text = ""

                Logger.d(TAG, "stationName = $stationName")

                searchFragmentPresenter.getMeasureInfos(context, stationName, addr)
                ll_edit_text_wrapper.requestFocus()
                rv_search_station_list.visibility = View.GONE
                tv_remove_all.visibility = View.GONE

                v_remove_text.visibility = if (et_search.text.isNotEmpty()) View.VISIBLE else View.INVISIBLE
            }
        })

        searchAdapter.setDeleteClick(object : SearchAdapter.DeleteClick {
            override fun onClick(view: View, position: Int) {
                searchFragmentPresenter.removeLatestStationData(position)
                searchAdapter.removeItem(position)
            }
        })

        rv_search_station_list.setOnTouchListener { v, _ ->
            imm.hideSoftInputFromWindow(v.windowToken, 0)
            false
        }

        rl_background.setOnTouchListener { v, _ ->
            imm.hideSoftInputFromWindow(v.windowToken, 0)
            false
        }

        v_remove_text.setOnClickListener {
            et_search.setText("")
            et_search.requestFocus()
            setLatestDataIfExist()
        }

        tv_remove_all.setOnClickListener {
            val removeDialog = AlertDialog.Builder(context)
            removeDialog.setTitle("전체삭제")
            removeDialog.setMessage("모든 검색 이력을 삭제합니다.")
            removeDialog.setCancelable(false)
            removeDialog.setPositiveButton("확인") { dialog, _ ->
                dialog.dismiss()
                val removeSize = searchFragmentPresenter.removeAllLatestStationData()
                searchAdapter.removeAllItem(removeSize)
                setLatestDataIfExist()
            }
            removeDialog.setNegativeButton("취소") { dialog, _ -> dialog.dismiss() }
            removeDialog.show()
        }

        et_search.addTextChangedListener(stationListTextWatcher)
        et_search.onFocusChangeListener = etSearchFocusListener

        setLatestDataIfExist()
    }

    private val stationListTextWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

        }

        override fun afterTextChanged(s: Editable) {
            val str = s.toString()
            if (str.isNotEmpty()) {
                Logger.d(TAG, "search text = " + s)
                Logger.d(TAG, "imageFragmentPresenter.getStationListByName(str) = " + searchFragmentPresenter.getStationListByName(str))
                searchAdapter.updateStationList(searchFragmentPresenter.getStationListByName(str), false)
                rv_search_station_list.visibility = View.VISIBLE
                tv_remove_all.visibility = View.GONE
                v_remove_text.visibility = View.VISIBLE
            } else {
                v_remove_text.visibility = View.INVISIBLE
                setLatestDataIfExist()
            }
        }
    }

    private fun setLatestDataIfExist() {
        val latestStationDatas = searchFragmentPresenter.getAllLatestStationData()
        val stationDatas = ArrayList<StationData>()

        if (latestStationDatas.isNotEmpty()) {
            for (latestStationData in latestStationDatas) {
                Logger.d(TAG, "stationName = ${latestStationData.stationName}")
                val stationData = StationData()
                stationData.stationName = latestStationData.stationName
                stationData.addr = latestStationData.addr

                stationDatas.add(stationData)
            }
            searchAdapter.updateStationList(stationDatas, true)

            rv_search_station_list.visibility = View.VISIBLE
            tv_remove_all.visibility = View.VISIBLE
        } else {
            searchAdapter.updateStationList(stationDatas, false)

            rv_search_station_list.visibility = View.GONE
            tv_remove_all.visibility = View.GONE
        }

        rl_info_wrapper.visibility = View.GONE
        tv_status.visibility = View.GONE
        v_remove_text.visibility = if (et_search.text.isNotEmpty()) View.VISIBLE else View.INVISIBLE
    }

    private val etSearchFocusListener = View.OnFocusChangeListener { _, gainFocus ->
        if (gainFocus) {
            rv_search_station_list.visibility = View.VISIBLE
            rl_info_wrapper.visibility = View.GONE
            tv_status.visibility = View.GONE
            v_remove_text.visibility = if (et_search.text.isNotEmpty()) View.VISIBLE else View.INVISIBLE
        }
    }

    override fun showProgress() {
        avi.smoothToShow()
    }

    override fun hideProgress() {
        avi.smoothToHide()
    }

    override fun showErrorAlert(message: String) {
        CustomSnackBar(rl_background, message).showWithAction("OK", Runnable {
//            searchFragmentPresenter.getMeasureInfos(context, lastStationName, lastAddr)

            et_search.requestFocus()
            imm.showSoftInput(et_search, InputMethodManager.SHOW_IMPLICIT)
            rv_search_station_list.visibility = View.GONE
            tv_remove_all.visibility = View.GONE

            v_remove_text.visibility = if (et_search.text.isNotEmpty()) View.VISIBLE else View.INVISIBLE
        })
    }

    override fun showFinishAlert(message: String) {
        CustomSnackBar(rl_background, message).showWithAction("OK", Runnable { activity.finish() })
    }

    override fun changeColor(station: String, measureTime: String, pm10: String, pm25: String) {

        val currValue = getStatus(pm10)
        val runnable = Runnable {
            hideProgress()
            rl_info_wrapper.visibility = View.VISIBLE
            tv_status.visibility = View.VISIBLE

            if (currValue == 0) {
                tv_pm10.text = "..."
                tv_pm25.text = "..."
            } else {
                setMeasurePm10Info(pm10)
                setMeasurePm25Info(pm25)
            }

            tv_station.text = "$station ${ResUtil.getString(context, R.string.append_after_station_name)}"
            tv_time.text = measureTime

            when (currValue) {
                0 -> tv_status.text = ResUtil.getString(context, R.string.nothing)
                1 -> tv_status.text = ResUtil.getString(context, R.string.good)
                2 -> tv_status.text = ResUtil.getString(context, R.string.soso)
                3 -> tv_status.text = ResUtil.getString(context, R.string.bad)
                4 -> tv_status.text = ResUtil.getString(context, R.string.hell)
            }
            setMeasureStatusAlpha()

            val background = rl_background.background as TransitionDrawable
            background.startTransition(Define.BACKGROUND_TRANSITION_DURATION)

            beforeValue = currValue

            Answers.getInstance().logContentView(ContentViewEvent()
                    .putCustomAttribute("SEARCH_region", station)
                    .putCustomAttribute(station + "_pm10", pm10)
                    .putCustomAttribute(station + "_pm25", pm25)
            )
        }

        MeasureDataSetting(context, beforeValue, currValue, rl_background, runnable)
    }

    private fun setMeasureStatusAlpha() {
        val anim = ObjectAnimator.ofFloat(tv_status, View.ALPHA, 0f, 1f)
        anim.duration = Define.TEXT_ANIMATION_TIME.toLong()
        anim.start()
    }

    private fun setMeasurePm10Info(pm10: String) {
        if (pm10.equals("-", ignoreCase = true)) {
            tv_pm10.text = pm10
        } else {
            val animator = ValueAnimator.ofInt(0, Integer.parseInt(pm10))
            animator.duration = 1500
            animator.addUpdateListener { animation -> tv_pm10.text = animation.animatedValue.toString() }
            animator.start()
        }
    }

    private fun setMeasurePm25Info(pm25: String) {
        if (pm25.equals("-", ignoreCase = true)) {
            tv_pm25.text = pm25
        } else {
            val animator = ValueAnimator.ofInt(0, Integer.parseInt(pm25))
            animator.duration = 1500
            animator.addUpdateListener { animation -> tv_pm25.text = animation.animatedValue.toString() }
            animator.start()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }
}
