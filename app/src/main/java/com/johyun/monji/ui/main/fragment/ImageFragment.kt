package com.johyun.monji.ui.main.fragment

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.StaggeredGridLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.johyun.monji.R
import com.johyun.monji.data.model.googleImage.GoogleImage
import com.johyun.monji.ui.base.BaseActivity
import com.johyun.monji.ui.base.BaseFragment
import com.johyun.monji.util.CustomSnackBar
import com.johyun.monji.util.NetworkUtil
import com.johyun.monji.util.ResUtil
import corecommon.ui.main.fragment.ImageFragmentView
import kotlinx.android.synthetic.main.fragment_image.*
import javax.inject.Inject

class ImageFragment : BaseFragment(), ImageFragmentView {

    @Inject
    lateinit var imageFragmentPresenter: ImageFragmentPresenter
    private lateinit var imageAdapter: ImageAdapter

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater!!.inflate(R.layout.fragment_image, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as BaseActivity).activityComponent.inject(this)
        imageFragmentPresenter.attachView(this)
        hideProgress()
        chkNetwork()
    }

    private fun chkNetwork() {
        if (!NetworkUtil.isInternetConnect(context)) {
            val snackBar = CustomSnackBar(rl_background, ResUtil.getString(context, R.string.connect_fail_network))
            snackBar.showWithAction("OK", Runnable { chkNetwork() })
        } else {
            v_remove_text_or_search.setOnClickListener { v -> doSearch(v) }

            et_search.addTextChangedListener(textWatcher)
            et_search.setOnEditorActionListener { v, _, _ ->
                doSearch(v)
                true
            }

            imageAdapter = ImageAdapter(context)
            rv_search_station_list.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
            rv_search_station_list.adapter = imageAdapter
        }
    }

    private fun doSearch(view: View) {
        (activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken, 0)

        val searchText = et_search.text.toString()
        if (searchText.isNotEmpty()) {
            imageFragmentPresenter.getImageList(searchText)
        } else {
            CustomSnackBar(rl_background, ResUtil.getString(context, R.string.please_typing)).show()

        }
    }

    private val textWatcher = object : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun afterTextChanged(editable: Editable) {

            if (et_search.text.toString().isNotEmpty()) {
                v_remove_text_or_search.visibility = View.VISIBLE
            } else {
                v_remove_text_or_search.visibility = View.GONE
            }
        }
    }

    override fun showProgress() {
        avi.smoothToShow()
    }

    override fun hideProgress() {
        avi.smoothToHide()
    }

    override fun showErrorAlert(message: String) {
        CustomSnackBar(rl_background, message).show()
    }

    override fun setImage(list: List<GoogleImage>) {
        imageAdapter.updateImageList(list)
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }
}
