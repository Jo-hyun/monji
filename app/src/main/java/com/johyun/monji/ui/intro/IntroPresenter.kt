package com.johyun.monji.ui.intro

import android.content.Context
import com.johyun.monji.R
import com.johyun.monji.data.model.stationList.StationList
import com.johyun.monji.ui.base.BasePresenter
import com.johyun.monji.util.Logger
import com.johyun.monji.util.ResUtil
import corecommon.data.DataManager
import corecommon.data.database.RealmService
import corecommon.data.http.ExceptionHandler
import corecommon.ui.intro.IntroMvpView
import rx.SingleSubscriber
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by JH on 2017. 7. 9..
 */

class IntroPresenter
@Inject constructor(private val dataManager: DataManager,
                    private val realmService: RealmService,
                    private val exceptionHandler: ExceptionHandler) : BasePresenter<IntroMvpView>() {

    private val TAG = IntroPresenter::class.java.simpleName
    private var subscription: Subscription? = null
    private var failCount503 = 0

    override fun detachView() {
        super.detachView()

        subscription!!.unsubscribe()
        realmService.closeRealm()
    }

    fun getStationListCount(context: Context) {
        mvpView!!.showProgress()

        subscription = dataManager.stationListCount
                .timeout(20, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleSubscriber<StationList>() {
                    override fun onSuccess(stationList: StationList) {
                        Logger.d(TAG, "stationList.totalCount =" + stationList.totalCount)

                        setStationListToDB(context, stationList.totalCount)
                    }

                    override fun onError(error: Throwable) {
                        mvpView!!.hideProgress()
                        Logger.d(TAG, error.toString())

                        exceptionHandler.handleHttpException(error,
                                object : ExceptionHandler.ExceptHandleListener {
                                    override fun onTimeoutException(errorMessage: String) {
                                        mvpView!!.showErrorAlert(errorMessage)
                                    }

                                    override fun onException(errorMessage: String) {
                                        mvpView!!.showErrorAlert(errorMessage)
                                    }

                                    override fun on503Exception(errorMessage: String) {
                                        when (failCount503) {
                                            0 -> {
                                                mvpView!!.showErrorAlert(errorMessage)
                                                failCount503 += 1
                                            }
                                            1 -> {
                                                mvpView!!.showErrorAlert(errorMessage)
                                                failCount503 += 1
                                            }
                                            2 -> mvpView!!.showFinishAlert(errorMessage)
                                        }

                                    }
                                })
                    }
                })
    }

    fun setStationListToDB(context: Context, count: String) {
        mvpView!!.showProgress()

        subscription = dataManager.getStationList(count)
                .timeout(20, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleSubscriber<StationList>() {
                    override fun onSuccess(stationList: StationList) {
                        Logger.d(TAG, stationList.list.toString())

                        if (stationList.list.isEmpty()) {
                            mvpView!!.showErrorAlert(ResUtil.getString(context, R.string.fail_get_station_info))
                        } else {
                            Logger.d(TAG, "stationList.list.size =" + stationList.list.size)
                            realmService.insertStationList(stationList.list)
                            mvpView!!.goMainActivity()
                        }
                    }

                    override fun onError(error: Throwable) {
                        mvpView!!.hideProgress()
                        Logger.d(TAG, error.toString())

                        exceptionHandler.handleHttpException(error,
                                object : ExceptionHandler.ExceptHandleListener {
                                    override fun onTimeoutException(errorMessage: String) {
                                        mvpView!!.showErrorAlert(errorMessage)
                                    }

                                    override fun onException(errorMessage: String) {
                                        mvpView!!.showErrorAlert(errorMessage)
                                    }

                                    override fun on503Exception(errorMessage: String) {
                                        when (failCount503) {
                                            0 -> {
                                                mvpView!!.showErrorAlert(errorMessage)
                                                failCount503 += 1
                                            }
                                            1 -> {
                                                mvpView!!.showErrorAlert(errorMessage)
                                                failCount503 += 1
                                            }
                                            2 -> mvpView!!.showFinishAlert(errorMessage)
                                        }

                                    }
                                })
                    }
                })
    }
}
