package com.johyun.monji.ui.main.fragment

import com.johyun.monji.data.model.googleImage.GoogleImages
import com.johyun.monji.ui.base.BasePresenter
import com.johyun.monji.util.Logger
import corecommon.data.DataManager
import corecommon.data.database.RealmService
import corecommon.data.http.ExceptionHandler
import corecommon.ui.main.fragment.ImageFragmentView
import rx.SingleSubscriber
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ImageFragmentPresenter
@Inject constructor(private val dataManager: DataManager,
                    private val realmService: RealmService,
                    private val exceptionHandler: ExceptionHandler) : BasePresenter<ImageFragmentView>() {

    private val TAG = ImageFragmentPresenter::class.java.simpleName
    private lateinit var subscription: Subscription

    override fun detachView() {
        super.detachView()

        subscription.unsubscribe()
        realmService.closeRealm()
    }

    fun getImageList(query: String) {
        mvpView!!.showProgress()

        subscription = dataManager.getImageList(query)
                .timeout(20, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleSubscriber<GoogleImages>() {
                    override fun onSuccess(googleImages: GoogleImages) {
                        mvpView!!.hideProgress()
                        Logger.d(TAG, googleImages.toString())

                        mvpView!!.setImage(googleImages.items)
                    }

                    override fun onError(error: Throwable) {
                        mvpView!!.hideProgress()
                        Logger.d(TAG, error.toString())

                        exceptionHandler.handleHttpException(error,
                                object : ExceptionHandler.ExceptHandleListener {
                                    override fun onTimeoutException(errorMessage: String) {
                                        mvpView!!.showErrorAlert(errorMessage)
                                    }

                                    override fun onException(errorMessage: String) {
                                        mvpView!!.showErrorAlert(errorMessage)
                                    }

                                    override fun on503Exception(errorMessage: String) {
                                        mvpView!!.showErrorAlert(errorMessage)
                                    }
                                })
                    }
                })
    }
}
