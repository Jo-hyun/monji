package corecommon.ui.intro

import corecommon.ui.base.MvpView

/**
 * Created by JH on 2017. 7. 9..
 */

interface IntroMvpView : MvpView {

    fun showProgress()
    fun hideProgress()
    fun showErrorAlert(message: String)
    fun showFinishAlert(message: String)
    fun goMainActivity()
}
