package corecommon.ui.main.fragment

import com.johyun.monji.data.model.googleImage.GoogleImage
import corecommon.ui.base.MvpView

/**
 * Created by JH on 2017. 7. 7..
 */

interface ImageFragmentView : MvpView {

    fun showProgress()
    fun hideProgress()
    fun showErrorAlert(message: String)
    fun setImage(list: List<GoogleImage>)
}
