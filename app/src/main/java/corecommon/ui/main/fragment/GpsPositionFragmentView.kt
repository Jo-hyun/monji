package corecommon.ui.main.fragment

import corecommon.ui.base.MvpView

/**
 * Created by JH on 2017. 7. 7..
 */

interface GpsPositionFragmentView : MvpView {

    fun showProgress()
    fun hideProgress()
    fun showErrorAlert(message: String)
    fun showFinishAlert(message: String)
    fun changeColor(station: String, measureTime: String, pm10: String, pm25: String)
}
