package corecommon.injector.component

import corecommon.data.DataManager
import corecommon.data.database.RealmService
import corecommon.data.http.ExceptionHandler
import corecommon.injector.module.ApplicationModule
import corecommon.injector.module.DatabaseModule
import corecommon.injector.module.ExceptionModule
import corecommon.injector.module.NetworkModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApplicationModule::class, ExceptionModule::class, DatabaseModule::class, NetworkModule::class))
interface ApplicationComponent {

    fun dataManager(): DataManager
    fun exceptionHandler(): ExceptionHandler
    fun realmService(): RealmService
}
