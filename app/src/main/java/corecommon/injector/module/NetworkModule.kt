package corecommon.injector.module

import corecommon.data.http.ApiService
import corecommon.data.http.ApiServiceFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NetworkModule {
    @Singleton
    @Provides
    fun provideApiService(): ApiService = ApiServiceFactory.makeApiService()
}
