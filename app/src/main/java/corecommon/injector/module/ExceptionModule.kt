package corecommon.injector.module

import corecommon.data.http.ExceptionHandler
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ExceptionModule {
    @Singleton
    @Provides
    fun provideExceptionHandler(): ExceptionHandler = ExceptionHandler()
}
