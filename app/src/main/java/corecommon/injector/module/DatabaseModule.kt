package corecommon.injector.module

import corecommon.data.database.RealmService
import dagger.Module
import dagger.Provides
import io.realm.Realm
import io.realm.RealmConfiguration

@Module
class DatabaseModule(private val configuration: RealmConfiguration) {

    @Provides
    fun provideRealmService(): RealmService = RealmService(Realm.getInstance(configuration))
}
