package corecommon

import android.app.Application
import android.content.Context
import com.crashlytics.android.Crashlytics
import com.tsengvn.typekit.Typekit
import corecommon.injector.component.ApplicationComponent
import corecommon.injector.component.DaggerApplicationComponent
import corecommon.injector.module.DatabaseModule
import io.fabric.sdk.android.Fabric
import io.realm.RealmConfiguration

class MonjiApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Fabric.with(this, Crashlytics())
        Typekit.getInstance()
                .addNormal(Typekit.createFromAsset(this, "fonts/NanumBarunGothicLight.otf"))
                .addBold(Typekit.createFromAsset(this, "fonts/NanumBarunGothicBold.otf"))
    }

    val applicationComponent: ApplicationComponent by lazy {
        val realmConfig = RealmConfiguration.Builder(applicationContext)
                .deleteRealmIfMigrationNeeded()
                .build()

        DaggerApplicationComponent
                .builder()
                .databaseModule(DatabaseModule(realmConfig))
//                .dataManagerModule(DataManagerModule(ApiServiceFactory.makeApiService()))
                .build()
    }

    companion object {
        operator fun get(context: Context): MonjiApplication = context.applicationContext as MonjiApplication
    }
}
