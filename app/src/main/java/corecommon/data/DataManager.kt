package corecommon.data

import com.johyun.monji.data.model.googleImage.GoogleImages
import com.johyun.monji.data.model.measureInfos.MeasureInfos
import com.johyun.monji.data.model.nearStations.NearStations
import com.johyun.monji.data.model.stationList.StationList
import com.johyun.monji.util.Define
import corecommon.data.http.ApiService
import rx.Single
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataManager @Inject constructor(private val apiService: ApiService) {

    fun getNearStations(tmX: Double, tmY: Double): Single<NearStations> {
        val queryMap = HashMap<String, String>()
        queryMap.put(Define.PARAM_TMX, tmX.toString())
        queryMap.put(Define.PARAM_TMY, tmY.toString())
        queryMap.put(Define.PARAM_RETURNTYPE, Define.JSON)
        queryMap.put(Define.PARAM_SERVICE_KEY, Define.SERVICE_KEY)
        queryMap.put(Define.PARAM_NUMOFROWS, "1") // we need nearest one

        return apiService.getNearStations(queryMap)
    }

    fun getMeasureInfos(stationName: String): Single<MeasureInfos> {
        val queryMap = HashMap<String, String>()
        queryMap.put(Define.PARAM_STATIONNAME, stationName)
        queryMap.put(Define.PARAM_DATATERM, Define.DATATERM_VALUE)
        queryMap.put(Define.PARAM_VER, Define.VER)
        queryMap.put(Define.PARAM_RETURNTYPE, Define.JSON)
        queryMap.put(Define.PARAM_SERVICE_KEY, Define.SERVICE_KEY)
        queryMap.put(Define.PARAM_NUMOFROWS, "1") // we need latest one

        return apiService.getMeasureInfo(queryMap)
    }

    val stationListCount: Single<StationList>
        get() {
            val queryMap = HashMap<String, String>()
            queryMap.put(Define.PARAM_RETURNTYPE, Define.JSON)
            queryMap.put(Define.PARAM_SERVICE_KEY, Define.SERVICE_KEY)
            queryMap.put(Define.PARAM_NUMOFROWS, "1") // count 1 for totalCount not real data

            return apiService.getStationList(queryMap)
        }

    fun getStationList(count: String): Single<StationList> {
        val queryMap = HashMap<String, String>()
        queryMap.put(Define.PARAM_RETURNTYPE, Define.JSON)
        queryMap.put(Define.PARAM_SERVICE_KEY, Define.SERVICE_KEY)
        queryMap.put(Define.PARAM_NUMOFROWS, count) //  for count of real data

        return apiService.getStationList(queryMap)
    }

    // 구글 이미지 검색
    fun getImageList(query: String): Single<GoogleImages> {
        val queryMap = HashMap<String, String>()
        queryMap.put(Define.PARAM_GOOGLE_IMAGE_SEARCH_Q, query)
        queryMap.put(Define.PARAM_GOOGLE_IMAGE_SEARCH_KEY, Define.PARAM_GOOGLE_IMAGE_SEARCH_KEY_VALUE)
        queryMap.put(Define.PARAM_GOOGLE_IMAGE_SEARCH_CX, Define.PARAM_GOOGLE_IMAGE_SEARCH_CX_VALUE)
        queryMap.put(Define.PARAM_GOOGLE_IMAGE_SEARCH_SEARCHTYPE, Define.PARAM_GOOGLE_IMAGE_SEARCH_SEARCHTYPE_VALUE)
        queryMap.put(Define.PARAM_GOOGLE_IMAGE_SEARCH_IMAGE_SIZE, Define.PARAM_GOOGLE_IMAGE_SEARCH_IMAGE_SIZE_VALUE)

        return apiService.getImageList(queryMap)
    }
}