package corecommon.data.http

import retrofit2.adapter.rxjava.HttpException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class ExceptionHandler {

    interface ExceptHandleListener {
        fun onTimeoutException(errorMessage: String)

        fun onException(errorMessage: String)

        fun on503Exception(errorMessage: String)
    }

    fun handleHttpException(exception: Throwable, exceptHandleListener: ExceptHandleListener) {

        when (exception) {
            is HttpException -> {
                when (exception.code()) {
                    400 -> exceptHandleListener.onTimeoutException("요청을 완료할 수 없습니다.")
                    503 -> exceptHandleListener.on503Exception("현재 서버를 사용할 수 없습니다.\n잠시 후 다시 시도해 주세요.")
                // ... more error code
                    else -> exceptHandleListener.onException("서버 통신중 오류가 발생 하였습니다.")
                }
            }
            is SocketTimeoutException -> exceptHandleListener.onTimeoutException("일시적인 네트워크 오류입니다.")
            is UnknownHostException -> exceptHandleListener.onException("오프라인 모드 입니다.")
            else -> exceptHandleListener.onException("오류가 발생했습니다.")
        }
    }
}
