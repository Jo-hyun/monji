package corecommon.data.http

import com.johyun.monji.data.model.googleImage.GoogleImages
import com.johyun.monji.data.model.measureInfos.MeasureInfos
import com.johyun.monji.data.model.nearStations.NearStations
import com.johyun.monji.data.model.stationList.StationList
import com.johyun.monji.util.Define

import retrofit2.http.GET
import retrofit2.http.QueryMap
import rx.Single

interface ApiService {

    // 대기질 측정소 정보를 조회하기 위한 서비스로 TM 좌표기반의 가까운 측정소 및 측정소 목록과 측정소의 정보를 조회할 수 있다.
    // serviceKey % 특수문자가 들어있어서 @QueryMap(encoded = true) 인코드 해줌
    @GET(Define.GET_NEAR_STATIONS)
    fun getNearStations(@QueryMap(encoded = true) options: Map<String, String>): Single<NearStations>

    // 측정소명과 측정데이터 기간(일, 한달, 3개월)으로 해당 측정소의 일반항목 측정정보를 제공하는 측정소별 실시간 측정정보 조회
    @GET(Define.GET_STATION_MEASURE_INFO)
    fun getMeasureInfo(@QueryMap(encoded = true) options: Map<String, String>): Single<MeasureInfos>

    // 측정소 주소 또는 측정소 명칭으로 측정소 목록 또는 단 건의 측정소 상세 정보를 제공하는 서비스
    @GET(Define.GET_STATION_LIST_BY_NAME)
    fun getStationList(@QueryMap(encoded = true) options: Map<String, String>): Single<StationList>

    // 구글 이미지 검색
    @GET(Define.GOOGLE_IMAGE_SEARCH_BASE_URL)
    fun getImageList(@QueryMap(encoded = false) options: Map<String, String>): Single<GoogleImages>
}
