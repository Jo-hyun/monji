package corecommon.data.http

import com.google.gson.Gson
import com.johyun.monji.BuildConfig
import com.johyun.monji.util.Define
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import okio.BufferedSource
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.net.ProtocolException

object ApiServiceFactory {

    fun makeApiService(): ApiService = makeService(makeOkHttpClient(makeLoggingInterceptor()))

    private fun makeService(okHttpClient: OkHttpClient): ApiService {
        val retrofit = Retrofit.Builder()
                .baseUrl(Define.BASE_URL)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(NullOnEmptyConverterFactory())
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .build()
        return retrofit.create(ApiService::class.java)
    }

    private fun makeOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        return OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .addNetworkInterceptor { chain ->
                    var response: Response
                    try {
                        response = chain.proceed(chain.request())
                    } catch (e: ProtocolException) {
                        response = Response.Builder()
                                .request(chain.request())
                                .code(204)
                                .body(object : ResponseBody() {
                                    override fun contentType(): MediaType? = null
                                    override fun contentLength(): Long = 0
                                    override fun source(): BufferedSource? = null
                                })
                                .protocol(Protocol.HTTP_1_1)
                                .build()
                    }

                    response
                }
                .build()
    }

    private fun makeLoggingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        return logging
    }
}
