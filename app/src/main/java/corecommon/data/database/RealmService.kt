package corecommon.data.database

import com.johyun.monji.data.database.LatestStationData
import com.johyun.monji.data.database.StationData
import com.johyun.monji.data.model.stationList.Station
import io.realm.Case
import io.realm.Realm
import java.util.*

class RealmService(val realm: Realm) {

    private val TAG = RealmService::class.java.simpleName

    val allStationList: List<StationData>
        get() = realm.where(StationData::class.java).findAll()

    fun getStationList(stationName: String): MutableList<StationData> {
        return realm
                .where(StationData::class.java)
                .contains("stationName", stationName, Case.INSENSITIVE)
                .or()
                .contains("addr", stationName, Case.INSENSITIVE)
                .findAll()
    }

    fun insertStationList(list: List<Station>) {

        realm.executeTransactionAsync({ realm ->
            val tempStationList = ArrayList<StationData>()

            for ((stationName, addr) in list) {
                tempStationList.add(StationData(stationName, addr))
            }

            realm.copyToRealmOrUpdate(tempStationList)
        }, { realm.close() }) { realm.close() }
    }

    val allLatestStationList: List<LatestStationData>
        get() = realm.where(LatestStationData::class.java).findAll()

    fun deleteLatestStation(position: Int) {
        val results = realm.where(LatestStationData::class.java).findAll()

        realm.executeTransaction {
            // 하나의 객체를 삭제
            val latestStationData = results[position]
            latestStationData.deleteFromRealm()
        }
    }

    fun deleteAllLatestStation(): Int {
        val results = realm.where(LatestStationData::class.java).findAll()

        realm.executeTransaction {
            // 전체 맞는 데이터를 삭제
            results.deleteAllFromRealm()
        }

        return results.size
    }

    fun insertLatestStationData(latestStationData: LatestStationData) {

        realm.executeTransaction { realm -> realm.copyToRealmOrUpdate(latestStationData) }
    }

    fun deleteAll() {
        realm.executeTransaction { realm -> realm.deleteAll() }
    }

    fun closeRealm() {
        realm.close()
    }
}
